package com.matritel.resttodo.controller;

import com.matritel.resttodo.model.Priority;
import com.matritel.resttodo.model.TodoDto;
import com.matritel.resttodo.repository.entity.TodoEntity;
import com.matritel.resttodo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
//If we use RestController we don't need to annotate ResponseBody for the methods
@RequestMapping(value = "api/matritel/todos")  //
public class TodoController {

    @Autowired
    private TodoService todoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    // IT's a good convention to use http requests as method name
    public TodoDto postTodo (@Valid @RequestBody TodoDto todoInput){
        return todoService.create(todoInput);
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TodoEntity getTodoById(@PathVariable String id){
        if (todoService.getTodo(id).isPresent()){
            return todoService.getTodo(id).get();
        }
        throw  new ResponseStatusException(HttpStatus.NOT_FOUND, "ToDo is not found by the given id!");
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<TodoEntity> getAllTodo (){
        return todoService.getAll();
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo (@PathVariable String id){
        todoService.deleteTodo(id);
    }

    @GetMapping(value = "/findByPriority")
    @ResponseStatus(HttpStatus.OK)
    public List<TodoEntity> getAllTodoByPriority (@RequestParam(name = "priority") Priority priority){
        return todoService.listTodos(priority);
    }

    //It is needed to handle when no any todo is found by personInCharge
    @GetMapping(value = "/findByPic")
    @ResponseStatus(HttpStatus.OK)
    public List<TodoEntity> getAllTodoByPic (@RequestParam(name = "personInCharge") String personInCharge){
        return todoService.listTodos(personInCharge);
    }
}
