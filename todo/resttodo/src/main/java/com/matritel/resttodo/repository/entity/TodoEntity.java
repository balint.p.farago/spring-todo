package com.matritel.resttodo.repository.entity;


import com.matritel.resttodo.model.Priority;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;

//This class is a DAO
@Entity(name = "todo_table") // we can give the table name as a parameter
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TodoEntity {

    @Id
    @GeneratedValue(generator = "uuid2") // this is a specific id generator in Hibernate
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "description", columnDefinition = "VARCHAR(255)")
    private String description;

    @Column(name = "person_in_charge", columnDefinition = "VARCHAR(40)")
    private String personInCharge;

    @Enumerated(EnumType.STRING)
    @Column(length = 8)  // here if we don't want to specify the length, the @Column is not needed because with the @Entity the spring will recognize that the fields are columns
    private Priority priority;

    @Column(name = "created_at")
    private LocalDate createdAt;
}
