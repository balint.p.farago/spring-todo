package com.matritel.resttodo.model;

//DTO: is responsible for transferring data
//DAO: is responsible for accessing (persisting) the data. In the service and controller layer we don't need to use DAO objects

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@Builder  //Lombok annotations
@NoArgsConstructor
@AllArgsConstructor
public class TodoDto {

    private String id;

    //@JsonProperty(required = true)  // this annotations means that this field is required when the user create a new TodoDto and it will be convert from JSON String to Java object
    @Size(max = 255)
    @NotNull  // it cannot be null means if the user does not give this value it will get a bad request
    private String description;

    //@JsonProperty(required = true)
    @Size(max = 40)
    @NotNull
    private String personInCharge;

    //@JsonProperty(required = true)
    @NotNull
    private Priority priority;

    private LocalDate createdAt;

}
